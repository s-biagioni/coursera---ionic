import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';

import { Comment } from '../../shared/comment';


/**
 * Generated class for the CommentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-comment',
  templateUrl: 'comment.html',
})
export class CommentPage {
    
  commentForm: FormGroup;

  comment: Comment;

  constructor(public navCtrl: NavController, public navParams: NavParams,
          public viewCtrl: ViewController,
          private formBuilder: FormBuilder ) {
      
     
      this.commentForm = this.formBuilder.group({
          author: ['', [Validators.required, Validators.minLength(3)]],
          rating: 5,
          comment: ['', [Validators.required, Validators.minLength(3)]],
        });
  }
  
  onSubmit() {
      this.comment = this.commentForm.value;
      
      var d = new Date();
      var n = d.toISOString();
      this.comment.date = n;
      
      let commentFromModal = this.comment;      
      this.viewCtrl.dismiss(commentFromModal);
  }
  
  dismiss() {
      let commentFromModal = this.comment;      
      this.viewCtrl.dismiss(commentFromModal);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommentPage');
  }

}
